
###   ABOUT   #############################################################################

Profile Blog Information

Author:

 Matt Farina, aka, mfer
   matt.farina@gmail.com
   http://www.mattfarina.com 

Requirements: Drupal 6.x

###   FEATURES   #############################################################################

- Adds fields for a Blog URL, Blog Title, and Blog Feed URL to user profiles.
- Displays the title and url on the user profile.
- Adds the feed to the aggregator module.
- Provides default settings for the aggregator feed configuration.

###   INSTALLATION   #############################################################################

1. Download and unzip the Profile Blog Information module into your modules directory.

3. Goto Administer > Site Building > Modules and enable Profile Blog Information.

4. Goto Administer > User Management > Profile Blog Information and configure the options.


###   CHANGELOG   #############################################################################

1.1, 2008-xx-xx
----------------------
- #291386 - Moved Title field on user form above blog url field.
- #309589: Fixed issue where RSS field title could not be changed.
- #318080: Fixed schema. Removed unneeded default => ''

1.0, 2008-08-04
----------------------
- Initial release


