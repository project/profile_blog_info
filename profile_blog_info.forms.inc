<?php

/**
 * @file
 * Provides the admin and profile forms.
 */

/**
 * Form builder; Generate a form to set default settings.
 */
function profile_blog_info_admin_form() {
  $period = drupal_map_assoc(array(900, 1800, 3600, 7200, 10800, 21600, 32400, 43200, 64800, 86400, 172800, 259200, 604800, 1209600, 2419200), 'format_interval');
  
  $form = array();
  
  $form['profile_blog_info_profile_category'] = array('#type' => 'textfield',
    '#title' => t('Profile Category'),
    '#default_value' => variable_get('profile_blog_info_profile_category', ''),
    '#autocomplete_path' => 'admin/user/profile/autocomplete',
    '#description' => t('The category the new field should be part of. Categories are used to group fields logically. An example category is "Personal information".'),
  );
  
  $form['profile_blog_info_fieldset'] = array('#type' => 'textfield',
    '#title' => t('Fieldset name'),
    '#default_value' => variable_get('profile_blog_info_fieldset', t('Blog Information')),
    '#autocomplete_path' => 'admin/user/profile/autocomplete',
    '#description' => t('The name of the fieldset containing the information about the RSS feed. An example would be "Blog information".'),
  );
  
  $form['profile_blog_info_blog'] = array('#type' => 'textfield',
    '#title' => t('Title field title'),
    '#default_value' => variable_get('profile_blog_info_blog', t('Blog or homepage')),
    '#description' => t('The title to display above the blog/homepage field.'),
  );
  
  $form['profile_blog_info_title'] = array('#type' => 'textfield',
    '#title' => t('Title field title'),
    '#default_value' => variable_get('profile_blog_info_title', t('Title')),
    '#description' => t('The title to display above the title field.'),
  );
  
  $form['profile_blog_info_rss'] = array('#type' => 'textfield',
    '#title' => t('RSS field title'),
    '#default_value' => variable_get('profile_blog_info_rss', t('RSS')),
    '#description' => t('The title to display above the RSS field.'),
  );
  
  $form['profile_blog_info_explanation'] = array('#type' => 'textarea',
    '#title' => t('Explanation'),
    '#default_value' => variable_get('profile_blog_info_explanation', ''),
    '#description' => t('An optional explanation to go with the new field. The explanation will be shown to the user.'),
  );
  
  $form['profile_blog_info_required'] = array('#type' => 'checkbox',
    '#title' => t('The user must enter a value.'),
    '#default_value' => variable_get('profile_blog_info_required', 0),
  );
  $form['profile_blog_info_register'] = array('#type' => 'checkbox',
    '#title' => t('Visible in user registration form.'),
    '#default_value' => variable_get('profile_blog_info_register', 0),
  );
  
  $form['profile_blog_info_refresh'] = array('#type' => 'select',
    '#title' => t('Default Update interval for new items'),
    '#default_value' => variable_get('profile_blog_info_refresh', 900),
    '#options' => $period,
    '#description' => t('The length of time between feed updates. (Requires a correctly configured <a href="@cron">cron maintenance task</a>.)', array('@cron' => url('admin/reports/status'))),
  );
  
  // Handling of categories:
  $options = array();
  $categories = db_query('SELECT cid, title FROM {aggregator_category} ORDER BY title');
  while ($category = db_fetch_object($categories)) {
    $options[$category->cid] = check_plain($category->title);
  }
  if ($options) {
    $form['profile_blog_info_category'] = array('#type' => 'checkboxes',
      '#title' => t('Default aggregator category for news items'),
      '#default_value' => variable_get('profile_blog_info_category', array()),
      '#options' => $options,
      '#description' => t('New feed items are automatically filed in the checked categories.'),
    );
  }
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'system_settings_form_submit';
  $form['#submit'][] = 'drupal_flush_all_caches';
  $form['#theme'] = 'system_settings_form';
  
  return $form;
}

function profile_blog_info_admin_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';

  // Exclude unnecessary elements.
  unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);

  foreach ($form_state['values'] as $key => $value) {
    if ($op == t('Reset to defaults')) {
      variable_del($key);
    }
    else {
      if (is_array($value) && isset($form_state['values']['array_filter'])) {
        $value = array_keys(array_filter($value));
      }
      variable_set($key, $value);
    }
  }
  if ($op == t('Reset to defaults')) {
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    drupal_set_message(t('The configuration options have been saved.'));
  }

  cache_clear_all();
  drupal_rebuild_theme_registry();
  menu_rebuild();
  return;
}